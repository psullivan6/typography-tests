const { aliases } = require('./configuration');

exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig({
    resolve: {
      alias: aliases,
    },
  });
};
