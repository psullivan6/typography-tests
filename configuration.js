const path = require('path');


exports.aliases = {
  Components: path.resolve(__dirname, './src/components'),
  Data: path.resolve(__dirname, './src/data'),
  Images: path.resolve(__dirname, './src/images'),
  Pages: path.resolve(__dirname, './src/pages'),
  Styles: path.resolve(__dirname, './src/styles'),
  Utilities: path.resolve(__dirname, './src/utilities'),
};
