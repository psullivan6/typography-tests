import React from "react";

import Layout from "../components/layout"
import SEO from "../components/seo"

// Components
import Chip from '../components/Chip';
import Picture from 'Components/Picture';

// Styles
import { Blockquote } from 'Components/Typography/styles';

const IndexPage = () => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />

    <section>
      <Chip>Label Here</Chip>
      <Chip>Category Name</Chip>
      <Chip>Small</Chip>
      <Chip>Lorem ipsum dolor sit</Chip>
    </section>

    <hr />

    <section>
      <Chip isDefault={false}>Label Here</Chip>
      <Chip isDefault={false}>Category Name</Chip>
      <Chip isDefault={false}>Small</Chip>
      <Chip isDefault={false}>Lorem ipsum dolor sit</Chip>
    </section>

    <hr />

    <h1>The Adventures of Sherlock Holmes and Other Things</h1>
    <h2>The paralysis of Analysis</h2>

    <p>For all the preposterous hat and the <strong>vacuous face, there was something noble</strong> in the simple faith of our visitor which compelled our respect. She laid her little bundle of papers upon the table and went her way, with a promise to come again whenever she might be summoned.</p>

    <Picture
      sources={[
        {
          src: 'https://source.unsplash.com/random',
          alt: 'Random Unsplash Image'
        },
        {
          src: 'https://source.unsplash.com/random?technology',
          media: '(min-width: 800px)'
        },
        {
          src: 'https://source.unsplash.com/random?nature',
          media: '(min-width: 1200px)'
        }
      ]}
    />

    <p>Sherlock Holmes sat silent for a few minutes with his fingertips still pressed together, his legs stretched out in front of him, and his gaze directed upward to the ceiling. Then he took down from the rack the old and oily clay pipe, which was to him as a counsellor, and, having lit it, he leaned back in his chair, with the thick blue cloud-wreaths spinning up from him, and a look of infinite languor in his face.</p>

    <h2>Another section here</h2>
    <p>&ldquo;Quite an interesting study, that maiden," he observed. "I found her more interesting than her little problem, which, by the way, is rather a trite one. You will find parallel cases, if you consult my index, in Andover in '77, and there was something of the sort at The Hague last year. Old as is the idea, however, there were one or two details which were new to me. But the maiden herself was most instructive."</p>
    <Blockquote>You appeared to read a good deal upon her which was quite invisible to You appeared to read a good deal upleon her which was quite invisible table</Blockquote>
    <p>"Not invisible but unnoticed, Watson. You did not know where to look, and so you missed all that was important. I can never bring you to realise the importance of sleeves, the suggestiveness of thumb-nails, or the great issues that may hang from a boot-lace. Now, what did you gather from that woman's appearance? Describe it."</p>
    <p>"Well, she had a slate-coloured, broad-brimmed straw hat, with a feather of a brickish red. Her jacket was black, with black beads sewn upon it, and a fringe of little black jet ornaments. Her dress was brown, rather darker than coffee colour, with a little purple plush at the neck and sleeves. Her gloves were greyish and were worn through at the right forefinger. Her boots I didn't observe. She had small round, hanging gold earrings, and a general air of being fairly well-to-do in a vulgar, comfortable, easy-going way."</p>
    <Blockquote>
      <p>It came to you crowned.</p>
      <p>I am tired of strawberry leaves.</p>
      <p>They become you.</p>
      <p>Only in public.</p>
    </Blockquote>
    <p>Upstairs, in his own room, Dorian Gray was lying on a sofa, with terror in every tingling fibre of his body. Life had suddenly become too hideous a burden for him to bear. The dreadful death of the unlucky beater, shot in the thicket like a wild animal, had seemed to him to pre-figure death for himself also. He had nearly swooned at what Lord Henry had said in a chance mood of cynical jesting.</p>
    <p>At five o'clock he rang his bell for his servant and gave him orders to pack his things for the night-express to town, and to have the brougham at the door by eight-thirty. He was determined not to sleep another night at Selby Royal. It was an ill-omened place. Death walked there in the sunlight. The grass of the forest had been spotted with blood.</p>

    <p>Then he wrote a note to Lord Henry, telling him that he was going up to town to consult his doctor and asking him to entertain his guests in his absence. As he was putting it into the envelope, a knock came to the door, and his valet informed him that the head-keeper wished to see him. He frowned and bit his lip. "Send him in," he muttered, after some moments' hesitation.</p>
  </Layout>
)

export default IndexPage
