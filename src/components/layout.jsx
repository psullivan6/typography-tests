/**
 * Layout component that queries for data
 * with Gatsby's StaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/static-query/
 */

import React, { Fragment } from "react"
import PropTypes from "prop-types"
import { StaticQuery, graphql } from "gatsby"
import Helmet from 'react-helmet';

// Styles
import GlobalStyle from '../styles/global';
import { Main } from '../styles/layout';

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <Fragment>
        <Helmet title={data.site.siteMetadata.title}>
          <html lang="en" />
          <link href="https://fonts.googleapis.com/css?family=Fira+Sans:200i,900i|Merriweather:300,400|Oswald:200,300,400,500,600,700" rel="stylesheet" />
        </Helmet>
        <GlobalStyle />
        <Main>{children}</Main>
      </Fragment>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
