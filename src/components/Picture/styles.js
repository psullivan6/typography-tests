import styled from 'styled-components';

export const Container = styled.picture`
  &,
  & img {
    display: block;
    max-width: 100%;
  }
`;

export default {
  Container,
}
