import React from 'react';

// Styles
import { Container } from './styles';


const Picture = ({ sources: [defaultSource, ...sources] }) => {
  console.log('sourceObjs', defaultSource, sources);

  return (
    <Container>
      {
        sources.map(sourceObj => <source srcSet={sourceObj.src} media={sourceObj.media} />)
      }
      <img src={defaultSource.src} alt={defaultSource.alt} />
    </Container>
  )
}

export default Picture;
