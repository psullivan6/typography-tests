import styled from 'styled-components';
import { rem } from 'polished';

export const Blockquote = styled.blockquote`
  position: relative;
  margin: ${rem(50)} ${rem(50)} ${rem(40)} ${rem(50)};

  &,
  & > * {
    font-size: ${rem(20)};
    font-weight: 400;
    line-height: ${rem(33)};
    font-style: italic;
  }

  &:before {
    content: "\\201C";
    z-index: -1;
    position: absolute;
    top: ${rem(10)};
    left: ${rem(-50)};
    font-size: ${rem(100)};
    color: #f9f;
  }

  &:after {
    content: "\\201D";
    z-index: -1;
    position: absolute;
    bottom: ${rem(-40)};
    right: ${rem(-20)};
    font-size: ${rem(100)};
    color: #f9f;
  }
`;

export default {
  Blockquote,
}