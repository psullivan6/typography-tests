import PropTypes from "prop-types";
import React from "react";

// Styles
import { Container } from './styles';

const Chip = ({ children, isDefault }) => (
  <Container isDefault={isDefault}>
    {children}
  </Container>
);

Chip.propTypes = {
  children: PropTypes.string,
};

Chip.defaultProps = {
  isDefault: true,
};

export default Chip;
