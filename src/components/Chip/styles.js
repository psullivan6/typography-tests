import styled from 'styled-components';
import { rem } from 'polished';

// Styles
import { dark } from 'Styles/colors';

export const Container = styled.div`
  display: inline-block;
  margin: 0.5em;
  padding: 0.5em 1em;
  border: ${rem(1)} solid ${dark};
  border-radius: 1rem;
  font-family: 'Oswald', sans-serif;
  font-size: ${rem(12)};
  font-weight: 400;
  line-height: 1;
  text-transform: uppercase;
  color: ${dark};

  &:first-of-type {
    margin-left: 0;
  }
`;

export default {
  Container,
}
