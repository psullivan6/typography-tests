// export const dark = '#00000f'; // #030303
// export const light = '#ffffcc';

export const light = '#00000f';
export const dark = '#64ccff';

export default {
  dark,
  light,
}
