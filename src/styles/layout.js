import styled from 'styled-components';

export const Main = styled.main`
  max-width: 42rem;
  margin-left: auto;
  margin-right: auto;
`;

export default {
  Main,
}
