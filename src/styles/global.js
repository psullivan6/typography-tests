import { createGlobalStyle } from 'styled-components'
import { rem } from 'polished';

// Styles
import { dark, light } from './colors';
import media from 'Styles/queries';

const copyBase = 18;

const GlobalStyle = createGlobalStyle`
  html,
  body {
    width: 100%;
  }

  html {
    box-sizing: border-box;
  }

  body {
    margin: 0;
    padding: ${rem(20)};
    background-color: ${light};
    font-family: 'Merriweather', serif;
    color: ${dark};
    -webkit-font-smoothing: antialiased;
    transition: padding 0.4s ease-in-out;

    ${media.small`
      padding: ${rem(30)};
    `}
    ${media.medium`
    padding: ${rem(60)};
    `}
    /* ${media.large`background: palevioletred;`} */
  }

  *, *:before, *:after {
    box-sizing: inherit;
  }

  ::selection {
    background: #fcf;
  }
  ::-moz-selection {
    background: #fcf;
  }


  /* ======================================================================== */
  /* UI */
  /* ======================================================================== */
  hr {
    height: 0;
    border: none;
    border-bottom: ${rem(1)} solid ${dark};
  }

  /* ======================================================================== */
  /* TYPOGRAPHY */
  /* ======================================================================== */
  p {
    font-size: ${rem(copyBase)};
    font-weight: 300;
    line-height: ${rem(Math.round(copyBase * 1.8))};
  }

  h1 {
    font-family: 'Fira Sans', sans-serif;
    font-size: ${rem(Math.round(copyBase * (10 / 3)))};
    line-height: 1;
  }

  h2 {
    font-family: 'Fira Sans', sans-serif;
    font-size: ${rem(Math.round(copyBase * 2))};
    font-weight: 200;
    font-style: italic;
    line-height: 1;
  }
`;

export default GlobalStyle;
